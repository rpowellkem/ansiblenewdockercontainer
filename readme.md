# Playbook for intial config of new docker container hosts

Update your ansible hosts file to include the ip's of new hosts under \[CentosContainersNew\]<br />
    - default hosts file is located at /etc/ansible/hosts

To run the playbook run the below code and enter the root password when prompted<br />
`ansible-playbook updatePackages.yaml --ask-pass`